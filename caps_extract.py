from calendar import monthrange
from datetime import datetime

from Config import caps_sql_conn_str, dw_sql_conn_str, risk_free_benchmark, risk_statistic_columns, \
    risk_statistic_benchmark_columns, label_column_header, column_order, output_file_location, filename
from dateutil.relativedelta import relativedelta

import pandas as pd
import pyodbc


# get most recent returns date from the database
def get_current_return_date():
    query = """
                SELECT MAX(ReturnDate)
                FROM [MCMWarehouse].[DM].[CompositeReturnsSummaryPivot]
                """

    cnxn = pyodbc.connect(dw_sql_conn_str)

    current_return_date = pd.read_sql(query, cnxn, params=[])
    current_return_date = current_return_date.iloc[0, 0]
    return current_return_date


# Gets mapping of composite codes/names to benchmark codes/names
def get_composite_benchmark_map():
    query = """
               SELECT *
                FROM [MCMWarehouse].[SRC].[CompositeBenchmarkMap]
                WHERE [IsInactive] = 0

                    """

    cnxn = pyodbc.connect(dw_sql_conn_str)

    df = pd.read_sql(query, cnxn, params=[])
    return df


# get all composite returns for the as of date
def get_composite_returns(as_of_date):
    query = """
                SELECT *
                FROM [MCMWarehouse].[DM].[CompositeReturnsSummaryPivot]
                WHERE ReturnDate = '%s'
                """ % as_of_date

    cnxn = pyodbc.connect(dw_sql_conn_str)

    df = pd.read_sql(query, cnxn, params=[])
    return df


def get_ytd_returns(returns_df, as_of_date):
    # get a list of only the current composites/benchmarks
    current_returns = tuple(returns_df.ReturnName.unique().tolist())

    query = """
                SELECT 
                    [CompositeName]
                    ,[CompositeCode]
                    ,[ReturnDate]
                    ,YEAR([ReturnDate]) AS ReturnYear
                    ,[ReturnName]
                    ,[ReturnType]
                    ,[ReturnValueType]
                    ,[YTD]
                FROM [MCMWarehouse].[DM].[CompositeReturnsSummaryPivot]
                WHERE ReturnDate <> '%s'
                AND ReturnName IN {0}
                AND MONTH(ReturnDate) = 12                
            """ .format(current_returns) % as_of_date

    cnxn = pyodbc.connect(dw_sql_conn_str)

    df = pd.read_sql(query, cnxn, params=[])
    return df


# calculates the look back date based on the returns date and look back period.  Adjusts for leap years
def calculate_start_date(returns_date, years_ago):
    start_date = returns_date - relativedelta(years=years_ago)

    # check for leap years, we will always need actual month end dates
    if start_date.month == 2 and monthrange(start_date.year, start_date.month)[1] == 29:
        start_date = start_date.replace(day=29)

    return start_date


# shapes risk data to be merged with the final performance dataframe
def shape_risk_statistics(comp_df, bench_df, period_label):
    period_label = str(period_label) + 'Y '
    comp_df = comp_df.melt(var_name=label_column_header, value_name="Port Performance(Gross)")
    comp_df[label_column_header] = period_label + comp_df[label_column_header]

    bench_df = bench_df.melt(var_name=label_column_header, value_name="Primary Benchmark")
    bench_df[label_column_header] = period_label + bench_df[label_column_header]

    return comp_df.merge(bench_df, on=label_column_header, how='left')


# use CAPS stored procedure to get risk statistics for each composite/bench
def get_risk_statistics(returns_date):
    map_df = get_composite_benchmark_map()
    return_years = [3, 5]
    risk_stat_df = pd.DataFrame([])

    for years in return_years:
        start_date = calculate_start_date(returns_date, years)

        for index, row in map_df.iterrows():
            # only try to get stats if inception date is older than the 3/5Y dates
            if row['CompositeInceptionDate'] <= start_date:
                comp_df, bench_df = \
                    get_risk_statistics_from_db(row['CompositeCode'], row['PrimaryBenchmarkCode'], start_date,
                                                returns_date)

                combined_df = shape_risk_statistics(comp_df, bench_df, years)

                # add in the other columns we care about
                combined_df["CompositeCode"] = row['CompositeCode']
                combined_df["FactsetCompositeCode"] = row['FactsetCompositeCode']
                combined_df["CompositeName"] = row['CompositeName']
                combined_df["ReturnDate"] = returns_date
                combined_df["Primary Benchmark Code"] = row['PrimaryBenchmarkCode']
                combined_df["PrimaryBenchmark"] = row['PrimaryBenchmark']
                combined_df["Secondary Benchmark Code"] = row['SecondaryBenchmarkCode']
                combined_df["SecondaryBenchmark"] = row['SecondaryBenchmark']
                risk_stat_df = risk_stat_df.append(combined_df)

    return risk_stat_df


# executes the the stored procedure provided by CAPS to compute Risk Statistics
def get_risk_statistics_from_db(composite, benchmark, start_date, end_date):
    query = """
                EXEC [CAPS].dbo.csp_Risk 
                    @Type = 'C'  -- c = composite
                    , @Freq = 'm'  -- m = monthly
                    , @CodeID = '%s'
                    , @IndexID = '%s'
                    , @RiskFreeID = '%s'
                    , @StartDate = '%s'
                    , @EndDate = '%s'
                    , @ClassID = 0
                    , @CatID = 1
                    , @XWR = 'AWR'
                    , @RptCurrID = 'USD'

            """ % (composite, benchmark, risk_free_benchmark, start_date, end_date)

    cnxn = pyodbc.connect(dw_sql_conn_str)

    # since the stored procedure returns 3 datasets we need to leverage a cursor
    crsr = cnxn.cursor()
    crsr.execute(query)

    # first result set is composite risk stats
    rows = crsr.fetchall()
    columns = [column[0] for column in crsr.description]
    if len(columns) > 1:
        composite_stats = pd.DataFrame.from_records(rows, columns=columns)

        # get only the columns we care about
        composite_stats = composite_stats[risk_statistic_columns]

        # second result set is benchmark risk stats, we don't need the 3rd
        if crsr.nextset():
            rows = crsr.fetchall()
            columns = [column[0] for column in crsr.description]
            benchmark_stats = pd.DataFrame.from_records(rows, columns=columns)
            # get only the columns we care about
            benchmark_stats = benchmark_stats[risk_statistic_benchmark_columns]
        else:
            benchmark_stats = pd.DataFrame()
    else:
        composite_stats = pd.DataFrame()
        benchmark_stats = pd.DataFrame()

    crsr.close()

    return composite_stats, benchmark_stats


def format_ytd_returns(ytd_df):
    ytd_gross_df = ytd_df[ytd_df['ReturnValueType'] == 'GrossValue']
    ytd_net_df = ytd_df[ytd_df['ReturnValueType'] == 'NetValue']

    ytd_gross_df = ytd_gross_df.melt(id_vars=["CompositeName", "CompositeCode", "ReturnDate", "ReturnYear",
                                              "ReturnName", "ReturnType", "ReturnValueType"],
                                     var_name=label_column_header,  value_name='Port Performance(Gross)')

    ytd_net_df = ytd_net_df.melt(id_vars=["CompositeName", "CompositeCode", "ReturnDate", "ReturnYear", "ReturnName",
                                          "ReturnType", "ReturnValueType"], var_name=label_column_header,
                                 value_name='Port Performance(Net)')

    ytd_gross_df[label_column_header] = ytd_gross_df["ReturnYear"]
    ytd_net_df[label_column_header] = ytd_net_df["ReturnYear"]

    ytd_gross_df = ytd_gross_df.drop(columns=['ReturnValueType', 'ReturnYear'])
    ytd_net_df = ytd_net_df.drop(columns=['ReturnValueType', 'ReturnYear'])

    return ytd_gross_df, ytd_net_df


# get performance and ytd performance
def get_returns(returns_date):
    returns_df = get_composite_returns(returns_date)
    returns_df = returns_df.rename(columns={"Inception": "ITD"})

    ytd_returns = get_ytd_returns(returns_df, returns_date)

    # separate into gross and net returns
    ytd_gross_df, ytd_net_df = format_ytd_returns(ytd_returns)
    gross_df = returns_df[returns_df['ReturnValueType'] == 'GrossValue']
    net_df = returns_df[returns_df['ReturnValueType'] == 'NetValue']

    gross_df = gross_df.melt(id_vars=["CompositeName", "CompositeCode", "ReturnDate", "ReturnName", "ReturnType",
                                      "ReturnValueType"], var_name=label_column_header,
                             value_name='Port Performance(Gross)')

    net_df = net_df.melt(id_vars=["CompositeName", "CompositeCode", "ReturnDate", "ReturnName", "ReturnType",
                                  "ReturnValueType"], var_name=label_column_header,
                         value_name='Port Performance(Net)')

    gross_df = gross_df.drop(columns=['ReturnValueType'])
    net_df = net_df.drop(columns=['ReturnValueType'])

    gross_df = pd.concat([gross_df, ytd_gross_df])
    net_df = pd.concat([net_df, ytd_net_df])

    # primary and secondary bench returns
    primary_bench_df = gross_df[gross_df['ReturnType'] == 'Primary Benchmark']
    primary_bench_df = primary_bench_df.rename(columns={"Port Performance(Gross)": "Primary Benchmark",
                                                        "ReturnName": "Primary Benchmark Code"})
    primary_bench_df = primary_bench_df.drop(columns=['ReturnType'])

    secondary_bench_df = gross_df[gross_df['ReturnType'] == 'Secondary Benchmark']
    secondary_bench_df = secondary_bench_df.rename(columns={"Port Performance(Gross)": "Secondary Benchmark",
                                                            "ReturnName": "Secondary Benchmark Code"})
    secondary_bench_df = secondary_bench_df.drop(columns=['ReturnType'])

    # Just get composite returns on net
    net_df = net_df[net_df['ReturnType'] == 'Composite']
    net_df = net_df.drop(columns=['ReturnType'])

    gross_df = gross_df[gross_df['ReturnType'] == 'Composite']
    gross_df = gross_df.drop(columns=['ReturnType'])

    # merge composite data back together
    final_df = pd.merge(gross_df, net_df, how="left", on=["CompositeName", "CompositeCode", "ReturnDate", "ReturnName",
                                                          label_column_header])

    # merge in primary benchmark
    final_df = pd.merge(final_df, primary_bench_df, how="left", on=["CompositeName", "CompositeCode", "ReturnDate",
                                                                    label_column_header])
    # merge in secondary benchmark
    final_df = pd.merge(final_df, secondary_bench_df, how="left", on=["CompositeName", "CompositeCode", "ReturnDate",
                                                                      label_column_header])
    final_df.dropna(subset=['Port Performance(Gross)'], inplace=True)

    # add in the benchmark names
    map_df = get_composite_benchmark_map()
    final_df = pd.merge(final_df, map_df[['CompositeCode', 'FactsetCompositeCode', 'PrimaryBenchmark',
                                          'SecondaryBenchmark']], on='CompositeCode', how='left')

    # get and append risk stats
    risk_stats_df = get_risk_statistics(returns_date)
    final_df = pd.concat([final_df, risk_stats_df])

    # add difference columns
    final_df["Primary Benchmark Difference"] = final_df["Port Performance(Gross)"] - final_df["Primary Benchmark"]
    final_df["Secondary Benchmark Difference"] = final_df["Port Performance(Gross)"] - final_df["Secondary Benchmark"]

    final_df = final_df.drop(columns=['ReturnName'])
    # Format Date
    final_df["ReturnDate"] = final_df["ReturnDate"].apply(lambda x: x.strftime('%Y%m%d')).astype(int)
    final_df["CompositeIndex"] = final_df["FactsetCompositeCode"] + " " + final_df["HEADER"].astype(str)

    final_df = final_df[column_order]
    return final_df


if __name__ == "__main__":
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)
    pd.set_option('display.max_colwidth', None)

    # by default get the most recent return date in the database, hard code if another date is required
    current_returns_date = get_current_return_date()
    # current_returns_date = datetime.strptime('3/31/2021', '%m/%d/%Y')
    returns_date_as_int = datetime.strftime(current_returns_date, '%Y%m%d')

    print("Getting Performance for " + str(current_returns_date))

    monthly_returns = get_returns(current_returns_date)
    monthly_returns.to_csv(output_file_location + filename + "-" + returns_date_as_int + ".csv", index=False)
