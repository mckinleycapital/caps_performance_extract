# CAPS connection string
caps_sql_conn_str = 'DRIVER={SQL Server Native Client 11.0};SERVER=DWSQLPROD;Trusted_Connection=yes;Database=%s;' \
                  % ("CAPS",)

dw_sql_conn_str = 'DRIVER={SQL Server Native Client 11.0};SERVER=DWSQLPROD;Trusted_Connection=yes;Database=%s;' \
                  % ("MCMWarehouse",)

output_file_location = "D:\\"
filename = "caps_performance_output"

risk_free_benchmark = 'sbmmtb3'

risk_statistic_columns = ["Beta", "Stdev", "INFO_Ratio", "Sharpe"]
risk_statistic_benchmark_columns = ["Stdev", "Sharpe"]

label_column_header = "HEADER"

column_order = ['CompositeName', 'CompositeCode', 'FactsetCompositeCode', 'ReturnDate', 'HEADER', 'CompositeIndex',
                'Port Performance(Gross)', 'Port Performance(Net)', 'Primary Benchmark Code', 'Primary Benchmark',
                'Secondary Benchmark Code', 'Secondary Benchmark', 'PrimaryBenchmark', 'SecondaryBenchmark',
                'Primary Benchmark Difference', 'Secondary Benchmark Difference']
